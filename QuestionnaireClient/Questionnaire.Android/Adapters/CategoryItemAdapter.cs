using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Questionnaire.Core.DataClasses;

namespace Questionnaire.Android
{
	class CategoryItemAdapter : BaseAdapter<Category> {
		protected Activity context = null;
		protected IList<Category> categories = null;

		public CategoryItemAdapter (Activity context, IList<Category> category) : base ()
		{
			this.context = context;
			this.categories = category;
		}

		public override Category this[int position]
		{
			get { return categories[position]; }
		}

		public override long GetItemId (int position)
		{
			return categories[position].Id;
		}

		public override int Count
		{
			get { return categories.Count; }
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			// Get our object for position
			var item = categories[position];			

			//Try to reuse convertView if it's not  null, otherwise inflate it from our item layout
			// gives us some performance gains by not always inflating a new view
			// will sound familiar to MonoTouch developers with UITableViewCell.DequeueReusableCell()
			var view = (convertView ?? context.LayoutInflater.Inflate(Resource.Layout.CategoryListItem, parent, false)) as LinearLayout;

			// Find references to each subview in the list item's view
			var txtTitle = view.FindViewById<TextView>(Resource.Id.txtTitle);

			//Assign item's values to the various subviews
			txtTitle.SetText (item.Title, TextView.BufferType.Spannable);

			//Finally return the view
			return view;
		}
	}
}

