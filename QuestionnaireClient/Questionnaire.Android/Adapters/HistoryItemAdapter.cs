using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Questionnaire.Core.DataClasses;

namespace Questionnaire.Android
{
	class HistoryItemAdapter : BaseAdapter<History> {
		protected Activity context = null;
		protected IList<History> historyItems = null;

		public HistoryItemAdapter (Activity context, IList<History> items) : base ()
		{
			this.context = context;
			this.historyItems = items;
		}

		public override History this[int position]
		{
			get { return historyItems[position]; }
		}

		public override long GetItemId (int position)
		{
			return position;
		}

		public override int Count
		{
			get { return historyItems.Count; }
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			// Get our object for position
			var item = historyItems[position];			

			//Try to reuse convertView if it's not  null, otherwise inflate it from our item layout
			// gives us some performance gains by not always inflating a new view
			// will sound familiar to MonoTouch developers with UITableViewCell.DequeueReusableCell()
			var view = (convertView ?? context.LayoutInflater.Inflate(Resource.Layout.HistoryListItem, parent, false)) as LinearLayout;

			view.FindViewById<TextView> (Resource.Id.txtDate).SetText (item.Date.ToString (), TextView.BufferType.Normal);
			view.FindViewById<TextView> (Resource.Id.txtCategory).SetText (item.CategoryName, TextView.BufferType.Normal);
			view.FindViewById<TextView> (Resource.Id.txtQTaken).SetText (item.QuestionsTaken.ToString(), TextView.BufferType.Normal);
			view.FindViewById<TextView> (Resource.Id.txtQRight).SetText (item.QuestionsRight.ToString(), TextView.BufferType.Normal);

			return view;
		}
	}
}

