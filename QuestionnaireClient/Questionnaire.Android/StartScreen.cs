using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Questionnaire.Core;
using Questionnaire.Core.Controllers;

namespace Questionnaire.Android
{
	[Activity (Label = "Questionnare", MainLauncher = true)]
	public class StartScreen : Activity
	{
		private Button startButton;
		private Button historyButton;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.StartPage);

			// Get our button from the layout resource,
			// and attach an event to it
			startButton = FindViewById<Button> (Resource.Id.btnStart);

			historyButton = FindViewById<Button> (Resource.Id.btnHistory);



			startButton.Click += HandleStartButtonClick;
			historyButton.Click += delegate {StartActivity (typeof(HistoryActivity));
			};

		}

		private async void HandleStartButtonClick(object sender, EventArgs e)
		{
			startButton.SetText ("Loading", TextView.BufferType.Normal);
			startButton.Enabled = false;

			await CategoryController.GetCategoriesAsync ();// Force this async method to run in sync, preload categories (controller caches values);
			StartActivity (typeof(CategorySelectActivity));	

			startButton.SetText ("Start", TextView.BufferType.Normal);
			startButton.Enabled = true;

		}

		protected override void OnResume ()
		{
			base.OnResume ();
			historyButton.Enabled = (HistoryController.GetHistory() != null);
		}



	}
}


