using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Questionnaire.Core.DataClasses;
using Questionnaire.Core;


namespace Questionnaire.Android
{
	[Activity (Label = "Category Select", NoHistory = true)]			
	public class CategorySelectActivity : Activity
	{
		IList<Category> categories;

		CategoryItemAdapter listAdapter;

		ListView categoryListView;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.CategorySelectPage);
			categoryListView = FindViewById<ListView> (Resource.Id.lstCategories);
			categoryListView.ItemClick += HandleCategoryClick;

		}

		private async void HandleCategoryClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			await Questionnaire.Core.Controllers.QuestionController.PrepareQuestionsAsync(e.Id);
			StartActivity(typeof(QuestionActivity));
		}


		protected override void OnResume ()
		{
			base.OnResume ();

			categories = Questionnaire.Core.Controllers.CategoryController.GetCategoriesAsync().Result.ToList();

			listAdapter = new Questionnaire.Android.CategoryItemAdapter(this, categories);

			//Hook up our adapter to our ListView
			categoryListView.Adapter = listAdapter;

		}
	}
}

