using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Questionnaire.Core.DataClasses;
using Questionnaire.Core.Controllers;
using Android.Graphics;
using Android.Graphics.Drawables;
using System.IO;
using System.Threading.Tasks;

namespace Questionnaire.Android
{
	[Activity (Label = "Question", NoHistory = true)]			
	public class QuestionActivity : Activity
	{
		private Question question;
		private RadioGroup radioGroup;
		private TextView questionText;
		private Button submitButton;

		private Dictionary <int, long> radioButtonMap;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.QuestionLayout);
			radioButtonMap = new Dictionary<int, long> ();

			this.question = QuestionController.GetCurrentQuestion();

			this.radioGroup = FindViewById<RadioGroup> (Resource.Id.rgAnswerOptions);

			this.questionText = FindViewById<TextView> (Resource.Id.txtQuestion);
			this.questionText.SetText (question.Text, TextView.BufferType.Normal);

			FillAnswerOptions ();

			FillQuestionImage ();

			this.submitButton = FindViewById<Button> (Resource.Id.btnSubmit);
			submitButton.Click += HandleSubmitButtonClick;

		}

		private void FillAnswerOptions ()
		{
			foreach (var answerOption in question.AnswerOptions.OrderBy(q => q.Id)) // A quick order to keep it tidy (and to know how stuff is gonna come out). Question order is not important, answer option is (kindof)
			{
				Button radioButton = new RadioButton (this);
				radioButton.Id = (int)answerOption.Id;
				radioButtonMap.Add (radioButton.Id, answerOption.Id);
				radioButton.SetText (answerOption.Text, TextView.BufferType.Normal);
				RadioGroup.LayoutParams lp = new RadioGroup.LayoutParams (RadioGroup.LayoutParams.MatchParent, RadioGroup.LayoutParams.WrapContent);
				radioGroup.AddView (radioButton, lp);
			}
		}

		private void FillQuestionImage ()
		{
			if (!string.IsNullOrWhiteSpace (question.ImageUrn)) 
			{
					// Need a better way of loading images... Currently this flips a table when trying to load from stream returned from async method
					var imageView = FindViewById<ImageView> (Resource.Id.imgQuestionImage);
					Drawable drawable = PictureDrawable.CreateFromStream (QuestionController.GetQuestionImageStream (), question.ImageUrn);
					imageView.SetImageDrawable (drawable);
			}
			else {
				var imageView = FindViewById<ImageView> (Resource.Id.imgQuestionImage);
				imageView.Visibility = ViewStates.Gone;
			}
		}

		private void HandleSubmitButtonClick (object sender, EventArgs e)
		{
			foreach (var item in radioButtonMap) {
				if (FindViewById<RadioButton> (item.Key).Checked) {
					QuestionController.SubmitAnswerAndMoveNext (item.Value);
					if (QuestionController.HasNextQuestion)
						StartActivity(typeof(QuestionActivity));//Kick off the next question;
					else
						StartActivity(typeof(HistoryActivity));//No more questions, go to history;


				}
			}
		}



	}
}

