using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Questionnaire.Core.DataClasses;
using Questionnaire.Core.Controllers;

namespace Questionnaire.Android
{
	[Activity (Label = "History")]	
	class HistoryActivity : Activity
	{
		private IEnumerable<History> historyItems;
		private ListView historyItemList;
		private HistoryItemAdapter listAdapter;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.HistoryLayout);
			this.historyItems = HistoryController.GetHistory ();
			historyItemList = FindViewById<ListView> (Resource.Id.lstHistory);
		}
			
			
		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			menu.Add (0, 1, 1, "Clear History");
			return base.OnCreateOptionsMenu (menu);
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			// Clear history menu item
			if (item.ItemId == 1) {
				HistoryController.ClearHistory ();
				Finish ();
			}
			return base.OnOptionsItemSelected (item);
		}

		protected override void OnResume ()
		{
			base.OnResume ();
			historyItems = (HistoryController.GetHistory () as IEnumerable<History>).OrderByDescending (item => item.Date); // Just to be safe - order them by date taken;

			listAdapter = new Questionnaire.Android.HistoryItemAdapter(this, historyItems.ToList());

			//Hook up our adapter to our ListView
			historyItemList.Adapter = listAdapter;
		}

	}
}

