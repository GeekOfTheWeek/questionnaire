﻿namespace Questionnaire.Core.DataClasses
{
    /// <summary>
    /// Question category
    /// </summary>
    public class Category
    {
        /// <summary>
        /// Identificator of category
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Title of category
        /// </summary>
        public string Title { get; set; }
    }
}