﻿using System;
using System.IO;
using Questionnaire.Core.Misc;

namespace Questionnaire.Core.DataClasses
{
    /// <summary>
    /// Config class. Used to store API and different configuration;
    /// </summary>
    internal class Config
    {
        /// <summary>
        /// Basepath for the API. Used to combine absolute paths.
        /// </summary>
        public static Uri ApiUri
        {
            get { return new Uri("http://10.0.2.2:44337/api/"); } // Localhost for android emulator
            //get { return new Uri("http://localhost:44337/api/"); } // Localhost for visual studio debugging
        }

        /// <summary>
        /// File location for history file.
        /// </summary>
        internal static string HistoryLocation
        {
            get { return Path.Combine(Helpers.GetLocalStorageDirectory(), "history.xml"); }
        }


    }
}