﻿using System.Collections.Generic;

namespace Questionnaire.Core.DataClasses
{
    public class Question
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public string ImageUrn { get; set; }
        public List<AnswerOption> AnswerOptions { get; set; }
        public Category Category { get; set; }
    }
}