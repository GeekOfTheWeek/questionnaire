﻿using System;

namespace Questionnaire.Core.DataClasses
{
    public class History
    {
        /// <summary>
        /// Count of question taken;
        /// </summary>
        public int QuestionsTaken { get; set; }

        /// <summary>
        /// Count of questions answered right;
        /// </summary>
        public int QuestionsRight { get; set; }

        /// <summary>
        /// Name of question category;
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// Date when the questionnaire was taken;
        /// </summary>
        public DateTime Date { get; set; }
    }
}