﻿namespace Questionnaire.Core.DataClasses
{
    /// <summary>
    /// Answer option for a question;
    /// </summary>
    public class AnswerOption
    {
        /// <summary>
        /// Identificator of answer option;
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Text of the answer option;
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Indicator of if this answer option is correct;
        /// </summary>
        public bool IsTure { get; set; }
    }
}