﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Questionnaire.Core.DataClasses;
using Questionnaire.Core.Providers;

namespace Questionnaire.Core.Controllers
{
    public class QuestionController
    {
        private static Question[] questions;
        private static int currentQuestionLocation;
        private static History history;
        
        /// <summary>
        /// Is there next question from prepared questions to be moved to;
        /// </summary>
        public static bool HasNextQuestion
        {
            get { return currentQuestionLocation < questions.Count(); }
        }

        /// <summary>
        /// Prepares questions to be retrieved with other methods
        /// </summary>
        /// <param name="categoryId">ID of category to prepare questions to</param>
        /// <returns></returns>
        public static async Task PrepareQuestionsAsync(long categoryId)
        {
            questions = await QuestionsProvider.GetQuestionsForCategoryAsync(categoryId);
            currentQuestionLocation = 0;
            history = new History();
            history.CategoryName = (await CategoryController.GetCategoriesAsync()).First(c => c.Id == categoryId).Title;
        }
        
        /// <summary>
        /// Gets current question. Questions must be initialised with PrepareQuestionsAsync(categoryId);
        /// </summary>
        /// <returns>Current question</returns>
        public static Question GetCurrentQuestion()
        {
            try
            {
                return questions[currentQuestionLocation];
            }
            catch (IndexOutOfRangeException)
            {
                return null;
            }
        }

        /// <summary>
        /// Submits answer to current question (returned by GetCurrentQuestion)
        /// </summary>
        /// <param name="answerOptionID">Answer option ID submited</param>
        /// <returns>Is submitted answer correct</returns>
        public static bool SubmitAnswerAndMoveNext(long answerOptionID)
        {
            if (questions == null)
            {
                throw new InvalidOperationException(
                    "Questions not initalized! To initialize call PrepareQuestions(long categoryId)");
            }

            bool isCorrect =
                GetCurrentQuestion().AnswerOptions.Any(option => option.Id == answerOptionID && option.IsTure);

            if (isCorrect)
            {
                history.QuestionsRight++;
            }

            currentQuestionLocation++;
            history.QuestionsTaken++;

            if (!HasNextQuestion)
            {
                // Handle history related things here;
                history.Date = DateTime.Now;
                HistoryController.AddItem(history);
            }

            return isCorrect;
        }

        /// <summary>
        /// Returns stream of image for current question;
        /// </summary>
        /// <returns></returns>
        public static Stream GetQuestionImageStream()
        {
            return ImageController.GetImageStream(questions[currentQuestionLocation].ImageUrn);
        }
    }
}