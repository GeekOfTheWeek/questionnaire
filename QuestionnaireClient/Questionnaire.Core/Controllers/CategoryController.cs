﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Questionnaire.Core.DataClasses;
using Questionnaire.Core.Providers;

namespace Questionnaire.Core.Controllers
{
    public class CategoryController
    {

        // Kind of a backing field and kind of cache thing-a-magic
        static IEnumerable<Category> _categories;

        /// <summary>
        /// Returns list of categories;
        /// </summary>
        /// <returns></returns>
        public static async Task<IEnumerable<Category>> GetCategoriesAsync()
        {
            
            return _categories ?? (_categories = await CategoriesProvider.GetCategoriesAsync());
        }
    }
}
