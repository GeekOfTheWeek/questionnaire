﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Questionnaire.Core.DataClasses;
using Questionnaire.Core.Providers;

namespace Questionnaire.Core.Controllers
{
    public class HistoryController
    {
        /// <summary>
        /// Clears local history
        /// </summary>
        public static void ClearHistory()
        {
            HistoryProvider.ClearHistory();
        }

        /// <summary>
        /// Returns all history items;
        /// </summary>
        /// <returns></returns>
        public static IList<History> GetHistory()
        {
            return HistoryProvider.GetHistory();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="historyItem"></param>
        public static void AddItem(History historyItem)
        {
            HistoryProvider.AddToHistory(historyItem);
            HistoryProvider.SaveHistory();
        }
    }
}
