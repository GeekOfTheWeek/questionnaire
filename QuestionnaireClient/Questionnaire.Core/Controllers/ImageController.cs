﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Questionnaire.Core.Providers;

namespace Questionnaire.Core.Controllers
{
    class ImageController
    {
        /// <summary>
        /// Returns stream of image for current question;
        /// </summary>
        /// <returns></returns>
        public static Stream GetImageStream(string imageUrn)
        {
            return ImageProvider.GetImageStream(imageUrn);
        }
    }
}
