﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Questionnaire.Core.Misc
{
    public class Helpers
    {

        /// <summary>
        /// Deserialized data from stream into object;
        /// </summary>
        /// <typeparam name="T">Type of object to be deserialized</typeparam>
        /// <param name="data">Data stream</param>
        /// <returns>Deserialized object of type T</returns>
        public static T DeserializeXml<T>(Stream data)
        {
            var serializer = new XmlSerializer(typeof (T));
            var reader = new NamespaceIgnorantXmlTextReader(new StreamReader(data));
            var deserializedData = (T) serializer.Deserialize(reader);
            return deserializedData;
        }

        /// <summary>
        /// Gets local storage directory;
        /// </summary>
        /// <returns></returns>
        public static string GetLocalStorageDirectory()
        {
            // TODO: Platform specific locations;
            return Path.GetDirectoryName(Environment.GetFolderPath(Environment.SpecialFolder.Personal));
        }
    }

    /// <summary>
    /// Namespace ignorant textreader - necessary to minimize risk of incountering unknown namespaces from HTTP responses
    /// </summary>
    internal class NamespaceIgnorantXmlTextReader : XmlTextReader
    {
        public NamespaceIgnorantXmlTextReader(TextReader reader) : base(reader)
        {
        }

        public override string NamespaceURI
        {
            get { return ""; }
        }
    }
}