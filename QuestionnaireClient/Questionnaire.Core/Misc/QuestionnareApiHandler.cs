﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Questionnaire.Core.DataClasses;

namespace Questionnaire.Core.Misc
{
    internal static class QuestionnareApiHandler
    {

        /// <summary>
        /// Gets HTTP request for relative path
        /// </summary>
        /// <param name="relPath"></param>
        /// <returns></returns>
        private static HttpWebRequest GetRequest(string relPath)
        {
            var uri = new Uri(Config.ApiUri, relPath);


            var req = (HttpWebRequest) WebRequest.Create(uri);

            if (!relPath.Trim().StartsWith("image"))
                req.Accept = "text/xml"; // Because expects does not work with IIS and for images will fail

            return req;
        }

        /// <summary>
        /// Gets HTTP response for relative path;
        /// </summary>
        /// <param name="relPath">Relative request path</param>
        /// <returns></returns>
        internal static async Task<HttpWebResponse> GetResponseAsync(string relPath)
        {
            HttpWebRequest req = GetRequest(relPath);


            try
            {
                Task<WebResponse> task = Task<WebResponse>.Factory.FromAsync(req.BeginGetResponse(null, null),
                                                                             req.EndGetResponse);

                var response = (HttpWebResponse) await task;

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return response;
                }

                throw new InvalidOperationException("Requested data could not be retireved");
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
        }

        /// <summary>
        /// Gets HTTP response for relative path; Image retrieval fails if run async :/
        /// </summary>
        /// <param name="relPath">Relative request path</param>
        /// <returns></returns>
        internal static HttpWebResponse GetResponse(string relPath)
        {
            HttpWebRequest req = GetRequest(relPath);

            try
            {
                var response = (HttpWebResponse) req.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return response;
                }

                throw new InvalidOperationException("Requested data could not be retireved");
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
        }

        /// <summary>
        /// Gets data from web API;
        /// </summary>
        /// <typeparam name="T">Data type expected from API</typeparam>
        /// <param name="relPath">Relative path on API</param>
        /// <returns>Deserialized object of API data</returns>
        public static async Task<T> GetDataAsync<T>(string relPath)
        {
            bool hasResponse = false;
            int retryCount = 0;
            HttpWebResponse response = null;

            do
            {
                try
                {
                    // Sometimes web api 500 errors out (probably VS2012 grabs .mdf file and opens it and makes it inaccessible to API. Retry a couple of times.
                    response = await GetResponseAsync(relPath);
                    hasResponse = true;
                }
                catch (Exception)
                {
                    retryCount++;
                    if (retryCount > 3)
                    {
                        throw;
                    }
                }
            } while (!hasResponse);

            var responseData = Helpers.DeserializeXml<T>(response.GetResponseStream());
            return responseData;
        }

        /// <summary>
        /// Gets data from web API; Sync call of the async method;
        /// </summary>
        /// <typeparam name="T">Data type expected from API</typeparam>
        /// <param name="relPath">Relative path on API</param>
        /// <returns>Deserialized object of API data</returns>
        
        public static T GetData<T>(string relPath)
        {
            return GetDataAsync<T>(relPath).Result; // Will block
        }

        /// <summary>
        /// Returns the data stream of API request;
        /// </summary>
        /// <param name="relPath">Relative path on API</param>
        /// <returns></returns>
        public static async Task<Stream> GetResponseStreamAsync(string relPath)
        {
            bool hasResponse = false;
            int retryCount = 1;
            HttpWebResponse response = null;

            do
            {
                try
                {
                    // Sometimes web api 500 errors out (probably VS2012 grabs .mdf file and opens it and makes it inaccessible to API. Retry a couple of times.
                    response = await GetResponseAsync(relPath);
                    hasResponse = true;
                }
                catch (Exception)
                {
                    retryCount++;
                    if (retryCount > 3)
                    {
                        throw;
                    }
                }
            } while (!hasResponse);

            // Image retrieval hangs without this
            var memStream = new MemoryStream();
            response.GetResponseStream().CopyTo(memStream);

            return memStream;
        }

        /// <summary>
        /// Returns the data stream of API request; Fully syncronous call;
        /// </summary>
        /// <param name="relPath">Relative path on API</param>
        /// <returns></returns>
        public static Stream GetResponseStream(string relPath)
        {
            return GetResponse(relPath).GetResponseStream();
        }
    }
}