﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Questionnaire.Core.DataClasses;
using Questionnaire.Core.Misc;

namespace Questionnaire.Core.Providers
{
    internal static class CategoriesProvider
    {
        private static IEnumerable<Category> categories;

        public static async Task<IEnumerable<Category>> GetCategoriesAsync()
        {
            return await GetCategoriesAsync(false);
        }


        public static async Task<IEnumerable<Category>> GetCategoriesAsync(bool refresh)
        {
            if (categories != null && !refresh)
            {
                return categories;
            }

            categories = await QuestionnareApiHandler.GetDataAsync<Category[]>("category/all");
            return categories;
        }
    }
}