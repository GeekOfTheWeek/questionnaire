using System.IO;
using Questionnaire.Core.Misc;

namespace Questionnaire.Core.Providers
{
    internal class ImageProvider
    {
        /// <summary>
        /// Returns stream of image (as returned from web API)
        /// </summary>
        /// <param name="imageUrn">Urn of the image</param>
        /// <returns></returns>
        public static Stream GetImageStream(string imageUrn)
        {
            return QuestionnareApiHandler.GetResponseStream("image/get/" + imageUrn);
        }
    }
}