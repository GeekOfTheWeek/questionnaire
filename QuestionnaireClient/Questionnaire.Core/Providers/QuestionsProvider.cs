using System.Threading.Tasks;
using Questionnaire.Core.DataClasses;
using Questionnaire.Core.Misc;

namespace Questionnaire.Core.Providers
{
    internal static class QuestionsProvider
    {
        /// <summary>
        /// Returns all questions available.
        /// </summary>
        /// <returns></returns>
        public static async Task<Question[]> GetQuestionsAsync()
        {
            return await QuestionnareApiHandler.GetDataAsync<Question[]>("question/all");
        }

        /// <summary>
        /// Returns all questions for specific category
        /// </summary>
        /// <param name="categoryId">Category ID of questions to be retrieved</param>
        /// <returns></returns>
        public static async Task<Question[]> GetQuestionsForCategoryAsync(long categoryId)
        {
            return
                await
                QuestionnareApiHandler.GetDataAsync<Question[]>(string.Format("question/category/{0}", categoryId));
        }

        /// <summary>
        /// Gets a single question
        /// </summary>
        /// <param name="questionId">ID of question</param>
        /// <returns></returns>
        public static async Task<Question> GetQuestion(long questionId)
        {
            return await QuestionnareApiHandler.GetDataAsync<Question>(string.Format("question/id/{0}", questionId));
        }
    }
}