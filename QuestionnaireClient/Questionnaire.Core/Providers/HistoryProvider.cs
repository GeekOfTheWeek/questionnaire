﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Questionnaire.Core.DataClasses;
using Questionnaire.Core.Misc;

namespace Questionnaire.Core.Providers
{
    internal static class HistoryProvider
    {
        private static IList<History> history;

        public static IList<History> GetHistory()
        {
            return GetHistory(false);
        }

        public static IList<History> GetHistory(bool refresh)
        {
            try
            {
                if (history != null & !refresh)
                {
                    return history;
                }

                using (var stream = new FileStream(Config.HistoryLocation, FileMode.Open))
                {
                    history = Helpers.DeserializeXml<List<History>>(stream);
                    return history;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static void SaveHistory()
        {
            var serializer = new XmlSerializer(typeof (List<History>));
            using (var stream = new FileStream(Config.HistoryLocation, FileMode.Create))
            {
                serializer.Serialize(stream, history);
            }
        }

        /// <summary>
        /// Adds to history. Call SaveHistory to persist;
        /// </summary>
        /// <param name="item">Item.</param>
        public static void AddToHistory(History item)
        {
            if (GetHistory(false) == null)
            {
                history = new List<History>();
            }
            history.Add(item);
        }

        /// <summary>
        /// Clears the history. Call SaveHistory to persist;
        /// </summary>
        public static void ClearHistory()
        {
            if (File.Exists(Config.HistoryLocation))
            {
                history = null;
                File.Delete(Config.HistoryLocation);
            }
        }
        
    }
}