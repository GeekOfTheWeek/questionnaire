﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using QuestionareServer.Models;

namespace QuestionareServer.Controllers.API
{
    public class QuestionController : ApiController
    {
        private QuestionnaireServerContext db = new QuestionnaireServerContext();

        // GET api/Question/all
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Question> GetQuestions()
        {
            //db.Database.Connection.Open();

            //var question = new Question()
            //    {
            //        Category = new Category() { Title = ".NET Questions" },
            //        Text = "Some question"
            //    };
            //question.AnswerOptions = new List<AnswerOption>();
            //var answerOption1 = db.AnswerOptions.Create();
            //answerOption1.IsTure = true;
            //answerOption1.Text = "question answeroption1";

            //question.AnswerOptions.Add(answerOption1);

            //var answerOption2 = db.AnswerOptions.Create();
            //answerOption2.IsTure = false;
            //answerOption2.Text = "question answeroption2";

            //question.AnswerOptions.Add(answerOption2);

            //var answerOption3 = db.AnswerOptions.Create();
            //answerOption3.IsTure = false;
            //answerOption3.Text = "question answeroption3";

            //question.AnswerOptions.Add(answerOption3);
            
            //db.Questions.Add(question);
            //db.SaveChanges();


            return db.Questions.AsEnumerable();
        }

        // GET api/Question/get/5
        [HttpGet]
        [ActionName("id")]
        public Question GetQuestion(long id)
        {
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            
            return question;
        }

        [HttpGet]
        [ActionName("category")]
        public IEnumerable<Question> GetQuestionsById(long id)
        {
            var questions = db.Questions.Where(q => q.CategoryId == id);
            return questions;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}