﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using QuestionareServer.Models;

namespace QuestionareServer.Controllers.API
{
    public class CategoryController : ApiController
    {
        private QuestionnaireServerContext db = new QuestionnaireServerContext();

        // GET api/category/all/
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Category> Get()
        {
            return db.Categories.AsEnumerable();
        }

        // GET api/category/id/5
        [HttpGet]
        [ActionName("id")]
        public Category Get(int id)
        {
            var category = db.Categories.Find(id);

            if (category == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return category;
        }

        
    }
}
