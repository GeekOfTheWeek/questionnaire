﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace QuestionareServer.Controllers.API
{
    public class ImageController : ApiController
    {
        // GET api/image/5
        [HttpGet]
        [ActionName("get")]
        public HttpResponseMessage Get(string id)
        {
            string basePath = HttpContext.Current.Server.MapPath("~/App_Data/Images/");
            string filePath = Path.Combine(basePath, id);
            
            

            if(File.Exists(filePath))
            {
                var response = new HttpResponseMessage();
                response.Content = new StreamContent(new FileStream(filePath,FileMode.Open)); // this file stream will be closed by lower layers of web api for you once the response is completed.
                string mediaType = string.Format("image/{0}", Path.GetExtension(filePath));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(mediaType);
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("image") {FileName = id};
                response.StatusCode = HttpStatusCode.OK;
                return response;
            }


            return Request.CreateResponse(HttpStatusCode.NotFound, "Image with URN " + id + "not found");


            
        }

        //public HttpResponseMessage GetImage()
        //{
        //    HttpResponseMessage response = new HttpResponseMessage();
        //    response.Content = new StreamContent(new FileStream(@"/App_Data/Images/" + id>)); // this file stream will be closed by lower layers of web api for you once the response is completed.
        //    response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
        //    response.Content.Headers.ContentDisposition.FileName()

        //    return response;
        //}

        // POST api/image
        public void Post([FromBody]string value)
        {
        }

        // PUT api/image/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/image/5
        public void Delete(int id)
        {
        }
    }
}
