﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace QuestionareServer.Models
{
    public class Question
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public string ImageUrn { get; set; }
        public virtual List<AnswerOption> AnswerOptions { get; set; }
        public virtual Category Category { get; set; }
        public virtual long CategoryId { get; set; }
    }
}