﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace QuestionareServer.Models
{
    public class AnswerOption
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public bool IsTure { get; set; }

        [IgnoreDataMember]
        public virtual Question Question { get; set; }
    }
}
