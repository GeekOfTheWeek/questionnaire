﻿using System.Data.Entity;

namespace QuestionareServer.Models
{
    public class QuestionnaireServerContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, add the following
        // code to the Application_Start method in your Global.asax file.
        // Note: this will destroy and re-create your database with every model change.
        // 
        // System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<QuestionareServer.Models.QuestionareServerContext>());

        public QuestionnaireServerContext() : base("name=QuestionnaireServerContext")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = true;
            // Eager load these sons of guns, so they show up when getting all questions;
            AnswerOptions.Load();
            Categories.Load();
        }

        public DbSet<Question> Questions { get; set; }
        public DbSet<AnswerOption> AnswerOptions { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}
